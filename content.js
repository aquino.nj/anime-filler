show_map = {
    "Naruto Shippuden": "naruto-shippuden",
    "One Piece": "one-piece"
}
filler_cache = {}

function getNotFiller(show_name) {
    if (filler_cache.hasOwnProperty(show_name)) {
        console.log("Used cache: " + show_name)
        return filler_cache[show_name]
    }
    console.log("No cache: " + show_name)
    return new Promise((resolve, reject) => {
        chrome.runtime.sendMessage({name: show_map[show_name]},
            response => {
                let not_filler = [];
                if (!response) {
                    console.log('message error: ', chrome.runtime.lastError.message);
                } else {

                    // Create Dummy DOM
                    var el = document.createElement('html');
                    el.innerHTML = response;

                    // Parse Dom for episode info
                    let table = el.getElementsByClassName('EpisodeList')[0].tBodies[0];
                    for (var i = 0, row; row = table.rows[i]; i++) {
                        if (row.className.includes('canon')) {
                            for (var j = 0, col; col = row.cells[j]; j++) {
                                if (col.classList.contains('Number')) {
                                    not_filler.push(col.textContent)
                                }
                            }
                        }
                    }
                    filler_cache[show_name] = not_filler
                }
                resolve(not_filler)
            })
    })
}

window.onload = async () => {
    const filler_warning = document.createElement("p");
    const node = document.createTextNode("This is filler.");
    filler_warning.appendChild(node);
    filler_warning.style.cssText = 'width: 100%;position: fixed;background-color: red;z-index: 100000;text-align: center;color: white;height: 2em;line-height: 2em;';
    filler_warning.setAttribute("hidden", true);
    document.body.appendChild(filler_warning);
    console.log("Loaded");
    setTimeout(function () {

        // Filler Warning
        video = document.getElementById("content-video-player");
        video.addEventListener('play', async () => {
            show = document.getElementsByClassName("PlayerMetadata__titleText")[0].textContent;
            episode = document.getElementsByClassName("PlayerMetadata__seasonEpisodeText")[0].textContent.split('E')[1];
            const not_filler = await getNotFiller(show);
            if (!not_filler.includes(episode)) {
                filler_warning.removeAttribute("hidden");
                console.log("Filler");
            } else {
                filler_warning.setAttribute("hidden", true);
                console.log("Not Filler");
            }
        });

        // Auto Skip the intro
        var observer = new MutationObserver(function (mutations) {
            mutations.forEach(function (mutationRecord) {
                if (mutationRecord.target.style.opacity == 1) {
                    console.log(mutationRecord.target.firstChild);
                    mutationRecord.target.firstChild.firstChild.click();
                }
            });
        });

        var target = document.getElementsByClassName('SkipButton');
        observer.observe(target[0].parentElement, {attributes: true, attributeFilter: ['style']});
    }, 3000);
};