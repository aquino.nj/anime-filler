filler_cache = {}

chrome.runtime.onMessage.addListener((request, sender, sendResponse) => {
    getFiller(request).then(sendResponse);
    return true; // return true to indicate you want to send a response asynchronously
});

async function getFiller(request) {
    let show = request.name;
    if (filler_cache.hasOwnProperty(show)) {
        console.log("Used cache: " + show)
        return filler_cache[show]
    }
    console.log("No cache: " + show)
    // Get anime filler html
    let response = await fetch("https://www.animefillerlist.com/shows/" + show, {});
    let text = await response.text();
    filler_cache[show] = text
    return text;
}