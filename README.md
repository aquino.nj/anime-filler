# Anime Filler

Chrome Extension for skipping anime filler

## Getting started

1. Navigate to Chrome [Extensions Page](chrome://extensions/)
2. Turn on Developer mode
3. Load unpacked extension

### Update Extension for custom Anime

1. Find show path on [animefillerlist.com](animefillerlist.com) (i.e `one-piece`)
2. Add mapping for Hulu name to `show_map` variable in `content.js`
```javascript
show_map = {
    "Naruto Shippuden": "naruto-shippuden",
    "One Piece": "one-piece"
}
```
3. Reload Chrome extension on [Extensions Page](chrome://extensions/)