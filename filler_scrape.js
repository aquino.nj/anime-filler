let show = "one-piece";

// Get anime filler html
let response = await fetch("https://www.animefillerlist.com/shows/" + show, {});
let text = await response.text();

// Create Dummy DOM
var el = document.createElement('html');
el.innerHTML = text;

// Parse Dom for episode info
let table = el.getElementsByClassName('EpisodeList')[0].tBodies[0];
let not_filler = [];
for (var i = 0, row; row = table.rows[i]; i++) {
    if (row.className.includes('canon')) {
        for (var j = 0, col; col = row.cells[j]; j++) {
            if (col.classList.contains('Number')) {
                not_filler.push(col.textContent)
            }
        }
    }
}

not_filler